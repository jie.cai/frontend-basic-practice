export default class Person {
  constructor(name, age, descption) {
    this.name = name;
    this.age = age;
    this.descption = descption;
  }

  introduce() {
    return `my name is ${this.name} ${this.age}yo and this is my resume/cv`;
  }
}
